﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calculator.aspx.cs" Inherits="Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Life is not a common Resource</title>
    <style type="text/css">
        .auto-style1 {
            width: 24%;
            height: 19px;
            border-style: solid;
            border-width: 1px;
        }
        .auto-style2 {
            height: 20px;
            width: 99px;
        }
        .auto-style3 {
            height: 20px;
            width: 92px;
        }
        .auto-style4 {
            height: 20px;
            width: 108px;
        }
        .auto-style5 {
            width: 25%;
            height: 159px;
            border: 1px solid #000000;
        }
        .auto-style6 {
            width: 53px;
        }
        .auto-style7 {
            margin-left: 725px;
        }
    </style>
</head>
<body style="width: 1921px; height: 326px; vertical-align:middle">
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TB" runat="server" Width="465px" align="center" Height="34px" CssClass="auto-style7"></asp:TextBox>
            <br />
            <table border="1" class="auto-style1" align="center">
                <tr>
                    <td class="auto-style3">
                        <asp:Button ID="BackSpaceBtn" runat="server" Text="Backspace" Width="94px" OnClick="BackSpaceBtn_Click" />
                    </td>
                    <td class="auto-style4">
                        <asp:Button ID="CEBtn" runat="server" Text="CE" Width="109px" OnClick="CEBtn_Click" />
                    </td>
                    <td class="auto-style2">
                        <asp:Button ID="CBtn" runat="server" Text="C" Width="99px" />
                    </td>
                </tr>
            </table>
            <table class="auto-style5" align="center">
                <tr>
                    <td>
                        <asp:Button ID="SevenBtn" runat="server" Text="7" Width="56px" OnClick="SevenBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="EightBtn" runat="server" Text="8" Width="63px" OnClick="EightBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="NineBtn" runat="server" Text="9" Width="53px" OnClick="NineBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="DevideBtn" runat="server" Text="/" Width="58px" OnClick="DevideBtn_Click" />
                    </td>
                    <td class="auto-style6">
                        <asp:Button ID="SqrtBtn" runat="server" Text="sqrt" Width="52px" OnClick="SqrtBtn_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="FourBtn" runat="server" Text="4" Width="55px" OnClick="FourBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="FiveBtn" runat="server" Text="5" Width="63px" OnClick="FiveBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="SixBtn" runat="server" Text="6" Width="54px" OnClick="SixBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="MultipBtn" runat="server" Text="*" Width="57px" OnClick="MultipBtn_Click" />
                    </td>
                    <td class="auto-style6">
                        <asp:Button ID="ModuluBtn" runat="server" Text="%" Width="51px" OnClick="ModuluBtn_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="OneBtn" runat="server" Text="1" Width="54px" OnClick="OneBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="TwoBtn" runat="server" Text="2" Width="61px" OnClick="TwoBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="ThreeBtn" runat="server" Text="3" Width="55px" OnClick="ThreeBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="MinusBtn" runat="server" Text="-" Width="55px" OnClick="MinusBtn_Click" />
                    </td>
                    <td class="auto-style6">
                        <asp:Button ID="DevBy1Btn" runat="server" Text="1/X" Width="52px" OnClick="DevBy1Btn_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ZeroBtn" runat="server" Height="26px" Text="0" Width="55px" OnClick="ZeroBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="PlusMinusBtn" runat="server" Text="+/-" Width="59px" OnClick="PlusMinusBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="DotBtn" runat="server" Text="." Width="56px" OnClick="DotBtn_Click" />
                    </td>
                    <td>
                        <asp:Button ID="PlusBtn" runat="server" Text="+" Width="54px" OnClick="PlusBtn_Click" />
                    </td>
                    <td class="auto-style6">
                        <asp:Button ID="EquelBtn" runat="server" Text="=" Width="53px" OnClick="EquelBtn_Click" />
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </form>
</body>
</html>
