﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Calculator : System.Web.UI.Page
{
    protected bool checkDot()
    {
        int calcFlag = 0;
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if(Text[i] == '+' || Text[i] == '-' || Text[i] == '/' || Text[i] == '*' || Text[i] == '%')
            {
                calcFlag = 1;
            }
            if(Text[i] == '.' && (calcFlag != 1))
            {
                return false;
            }
        }
        return true;
    }
    protected bool checkPlus()
    {
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if(Text[i] == '+')
            {
                return false;
            }
        }
        return true;
    }
    protected bool checkMinus()
    {
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if (Text[i] == '-')
            {
                return false;
            }
        }
        return true;
    }
    protected bool checkMult()
    {
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if (Text[i] == '*')
            {
                return false;
            }
        }
        return true;
    }
    protected bool checkDev()
    {
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if (Text[i] == '/')
            {
                return false;
            }
        }
        return true;
    }
    protected bool checkMod()
    {
        char[] Text = new char[this.TB.Text.Length];
        for (int i = 0; i < Text.GetLength(0); i++)
        {
            Text[i] = this.TB.Text[i];
        }

        for (int i = 0; i < Text.GetLength(0); i++)
        {
            if (Text[i] == '%')
            {
                return false;
            }
        }
        return true;
    }
    protected void DoCommand(string Text)
    {
        double num1 = 0;
        double num2 = 0;
        char[] TextArr = new char[Text.Length];
        for (int i = 0; i < Text.Length; i++)
        {
            TextArr[i] = Text[i];
        }

        for (int i = 0; i < Text.Length; i++)
        {
            switch(TextArr[i])
            {
                case '+':
                    num1 = double.Parse(Text.Substring(0, i));
                    num2 = double.Parse(Text.Substring(i + 1, (Text.Length) - (i + 1)));
                    this.TB.Text = (num1 + num2).ToString();
                    break;

                case '-':
                    num1 = double.Parse(Text.Substring(0, i));
                    num2 = double.Parse(Text.Substring(i + 1, (Text.Length) - (i + 1)));
                    this.TB.Text = (num1 - num2).ToString();
                    break;
                case '*':
                    num1 = double.Parse(Text.Substring(0, i));
                    num2 = double.Parse(Text.Substring(i + 1, (Text.Length) - (i + 1)));
                    this.TB.Text = (num1 * num2).ToString();
                    break;
                case '/':
                    num1 = double.Parse(Text.Substring(0, i));
                    num2 = double.Parse(Text.Substring(i + 1, (Text.Length) - (i + 1)));
                    this.TB.Text = (num1 / num2).ToString();
                    break;
                case '%':
                    num1 = double.Parse(Text.Substring(0, i));
                    num2 = double.Parse(Text.Substring(i + 1, (Text.Length) - (i + 1)));
                    this.TB.Text = (num1 % num2).ToString();
                    break;
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void ZeroBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '0'; 
    }

    protected void OneBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '1';
    }
    protected void TwoBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '2';
    }
    protected void ThreeBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '3';
    }
    protected void FourBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '4';
    }
    protected void FiveBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '5';
    }
    protected void SixBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '6';
    }
    protected void SevenBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '7';
    }
    protected void EightBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '8';
    } 
    protected void NineBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text += '9';
    }
    protected void DotBtn_Click(object sender, EventArgs e)
    {
        if(checkDot())
        this.TB.Text += '.';
    }
    protected void DevideBtn_Click(object sender, EventArgs e)
    {
        if(checkDev())
            this.TB.Text += '/';
    }
    protected void MultipBtn_Click(object sender, EventArgs e)
    {
        if(checkMult())
            this.TB.Text += '*';
    }
    protected void MinusBtn_Click(object sender, EventArgs e)
    {
        if(checkMinus())
            this.TB.Text += '-';
    }
    protected void PlusBtn_Click(object sender, EventArgs e)
    {
        if(checkPlus())
            this.TB.Text += '+';
    }
    protected void EquelBtn_Click(object sender, EventArgs e)
    {
        DoCommand(this.TB.Text);
    }
    protected void DevBy1Btn_Click(object sender, EventArgs e)
    {
        this.TB.Text = (1 / double.Parse(this.TB.Text)).ToString();
    }
    protected void ModuluBtn_Click(object sender, EventArgs e)
    {
        if(checkMod())
            this.TB.Text += '%';
    }
    protected void SqrtBtn_Click(object sender, EventArgs e)
    {
        double sqrtVal = double.Parse(this.TB.Text);
        if (sqrtVal >= 0)
        {
            this.TB.Text = (Math.Pow(sqrtVal, 0.5)).ToString();
        }
        else
        {
            this.TB.Text = "Math Error!";
        }
    }
    protected void PlusMinusBtn_Click(object sender, EventArgs e)
    {
        double newVal = double.Parse(this.TB.Text) * -1;
        this.TB.Text = newVal.ToString();
    }
    protected void BackSpaceBtn_Click(object sender, EventArgs e)
    {
        if (this.TB.Text.Length >= 1)
        {
            char[] Text = new char[this.TB.Text.Length - 1];

            for (int i = 0; i < Text.GetLength(0); i++)
            {
                Text[i] = this.TB.Text[i];
            }
            this.TB.Text = "";
            for (int i = 0; i < Text.GetLength(0); i++)
            {
                this.TB.Text += Text[i];
            }
        }
        else
        {
            this.TB.Text = "";
        }
    }
    protected void CEBtn_Click(object sender, EventArgs e)
    {
        this.TB.Text = "";
    }
}